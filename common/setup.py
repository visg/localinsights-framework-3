"""
write the description of bot_framework
"""
import re
import ast
from setuptools import setup


_version_re = re.compile(r'__version__\s+=\s+(.*)')

with open('scraper/__init__.py', 'rb') as f:
    version = str(ast.literal_eval(_version_re.search(
        f.read().decode('utf-8')).group(1)))


setup(
    name='scraper',
    version=version,
    url='https://bitbucket.org/joydeep314/bots',
    license='BSD', # fix a license
    author='Joydeep Bhattacharjee',
    author_email='joydeepubuntu@gmail.com',
    description='A bot framework to make the creation of'
                'new bots fast and easy',
    long_description=__doc__,
    packages=['scraper'],
    include_package_data=True,
    zip_safe=False,
    platforms='any',
    install_requires=[
        "beautifulsoup4>=4.5.3",
        "DateTime>=4.1.1",
        "python-dateutil>=2.6.0",
        "pytz>=2016.10",
        "six>=1.10.0",
        "zope.interface>=4.3.3",
        "requests>=2.13.0",
    ],
    classifiers=[
        'Development Status :: Planning',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: ',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
)


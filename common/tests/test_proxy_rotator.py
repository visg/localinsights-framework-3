from scraper.proxy_rotator import ProxyRotator

import unittest


class TestProxyRotator(unittest.TestCase):

    def test_get_proxy(self):
        pr = ProxyRotator()
        res = pr.proxies
        self.assertIn("http://locinsights",
                      res["http"],
                      "http gives a web url")
        self.assertIn("http://locinsights",
                      res["https"],
                      "https has a valid proxy")


if __name__ == "__main__":
    unittest.main()
